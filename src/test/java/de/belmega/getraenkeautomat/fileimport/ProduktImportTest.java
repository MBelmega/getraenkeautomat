package de.belmega.getraenkeautomat.fileimport;

import de.belmega.getraenkeautomat.Coin;
import de.belmega.getraenkeautomat.CoinType;
import de.belmega.getraenkeautomat.fileimport.ProduktImport;
import de.belmega.getraenkeautomat.utils.FileUtil;
import de.belmega.getraenkeautomat.Getränk;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ProduktImportTest {

    /**
     * Teste, dass die gesamte Datei mit 243 Zeichen eingelesen wird.
     *
     * @throws Exception
     */
    @Test
    public void testThatFileContentIsReadAsString() throws Exception {
        //arrange
        ProduktImport produktImport = new ProduktImport();
        File jsonFile = new File("produkt.json");


        //act
        String fileContent = FileUtil.readFile(jsonFile);


        //assert
        assertEquals(327, fileContent.length());
    }

    /**
     * Als nächstes Schreiben einer Methode, die Produkte innerhalb der gelesenenDatei findet.
     * Dazu nutzen wir erstmal ein einfaches Beispiel: Nur ein einzelnenes Produkt, innerhalb mit eckigen Klammern.
     * Methode muss also die eckige Klammern wegschneiden können, und nur das Produkt übrig lassen
     *
     * @throws Exception
     */
    @Test
    public void testThatProductIsFoundInString() throws Exception {
        //arrange
        String content = "[ {\"id\": 1, \"bezeichnung\" : \"Wasser\" , \"preis\" :  1.60}  ]";

        String expectedResult = "{\"id\": 1, \"bezeichnung\" : \"Wasser\" , \"preis\" :  1.60}";


        //act
        List<String> productsInString = new ProduktImport().findProductsInString(content);


        //assert
        assertEquals(1, productsInString.size());
        assertEquals(expectedResult, productsInString.get(0)); //Expection immer vorne!

    }

    @Test
    public void testThatTwoProductsFoundInString() throws Exception {
        //arrange
        String content = "\"produkte\" : [\n" +
                "\t\t{ \"id\": 1, \"bezeichnung\" : \"Wasser\", \"preis\" : 1.60 },\n" +
                "\t\t{ \"id\": 2, \"bezeichnung\" : \"Spezi\", \"preis\" : 1.80 }]";
        List<String> expectedResult = Arrays.asList(
                "{ \"id\": 1, \"bezeichnung\" : \"Wasser\", \"preis\" : 1.60 }",
                "{ \"id\": 2, \"bezeichnung\" : \"Spezi\", \"preis\" : 1.80 }"


        );

        //act

        List<String> productsInString = new ProduktImport().findProductsInString(content);
        //assert
        assertEquals(expectedResult.get(0), productsInString.get(0));
        assertEquals(expectedResult.get(1), productsInString.get(1));

    }

    @Test
    public void testThatImporterFindsThreeProductsinFile() throws Exception {
        //arrange
        ProduktImport produktImport = new ProduktImport();


        //act
        List<Getränk> getränke = produktImport.importiereProdukte("produkt.json");


        //assert
        assertEquals(5, getränke.size());
    }

    @Test
    public void testThatProductStringIsParsedToGetränk() throws Exception {
        //arrange
        ProduktImport produktImport = new ProduktImport();
        String productString = "{ \"id\": 1, \"bezeichnung\" : \"Wasser\", \"preis\" : 1.60 }";


        //act
        Getränk getränk = produktImport.createGetraenkFromString(productString);


        //assert
        assertEquals("Wasser", getränk.getBezeichnung());
        assertEquals(160, getränk.getPreis());
    }
}
