package de.belmega.getraenkeautomat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CoinTest {
    @Test
    public void testThat10ctComparedTo20ctIsMinus1
            () throws Exception {
        //arrange
        Coin tenCents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);
        Coin twentyCents = new Coin ("20 Cents", "20ct", 20, CoinType.COIN);


        //act
        int result = tenCents.compareTo(twentyCents);


        //assert
        assertEquals(-1, result);
    }

    @Test
    public void testThat10ctComparedTo10ctIs0() throws Exception {
        //arrange
        Coin tenCents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);
        Coin another10Cents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);

        //act
        int result = tenCents.compareTo(another10Cents);


        //assert
        assertEquals(0, result);
    }
    @Test
    public void testThat10ctComparedTo5ctIs1() throws Exception {
        //arrange
        Coin tenCents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);
        Coin fiveCents = new Coin ("5 Cents", "5ct", 5, CoinType.COIN);
        //act
        int result = tenCents.compareTo(fiveCents);

        //assert
        assertEquals(1, result);
    }
    //Test oben sind dazu da, bereits bestehende Logig zu testen -> compareTo()war schon  implementiert
    //Jetzt kommt ein Test, der die equals() - Methoden testet, die noch gar nicht implementiert ist
    // das nennt man Test Driven Development(TDD)
    @Test
    public void testThat10ctEqualsAnother10ct() throws Exception {
        //arrange
        Coin tenCents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);
        Coin another10Cents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);

        //act
        boolean equal = tenCents.equals(another10Cents); // Testet jetzt die neue equals() - Methode von Coin


        //assert
        assertTrue(equal);
    }
    @Test
    public void testThat10ctDoesNotEqual15ct() throws Exception {
        //arrange
        Coin tenCents = new Coin ("10 Cents", "10ct", 10, CoinType.COIN);
        Coin fiveCents = new Coin ("5 Cents", "5ct", 5, CoinType.COIN); //SOlange mintestens einer der vier Werte unterschiedlich ist, sind die Coins nicht equal. Egal welcher

        //act
        boolean equal = tenCents.equals(fiveCents);

        //assert
        assertFalse(equal); // Diese beiden Coins sind natürlich nicht equal
    }



}
