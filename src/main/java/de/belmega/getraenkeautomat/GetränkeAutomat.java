package de.belmega.getraenkeautomat;
import de.belmega.getraenkeautomat.fileimport.ProduktImport;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * 1. Automat gibt verschiedene Getränke als Liste aus ( Fanta (1,60), Wasser (1,00), Spezi (1,80), Saft (2,50).
 * 2. Nutzer kann durch Drücken einer Nummertaste ein Getränk wählen. Automat gibt gewähltes Getränk als Meldung aus.
 * 3. Nach jeder Aktion kehrt der Automat zur Getränkeliste zurück. Der Automat kann durch die Eingabe der Zahl 99 beendet werden.
 * <p>
 * 4. Der Automat akzeptiert folgende Münzen und Scheine: 5ct, 10ct, 20,ct, 50ct, 1eur, 2eur, 5eur, 10eur, 20eur
 * 5. Der Kunde bezahlt die gewählten Getränke, indm er nacheinander Münzen und Scheine eingibt
 * 6. Sobald das eingegebeme Geld den Preis des Getränks erreicht oder übersteigt, gibt der Automat den Fahrschein aus.
 * <p>
 * 7. Der Automat gibt das passende Wechselgeld
 * 8. Der Nutzer kann den Vorgang abbrechen, in dem er "abbrechen" eingibt.
 * 9. Wenn der Automat beendet wird, gibt er aus, wieviel Geld er bisher eingenommen hat.
 * <p>
 * 10.Die Anzeige von Geldbeträgen soll immer in Euro erfolgen, mit zwei Nachkommastellen
 * 11.Der Automat gibt als Wechselgeld keine Scheine heraus(5eur, 10eur, 20eur).
 * <p>
 * 12.Die angebotenen Produkte sollen bei Programmstart aus einer Datei ausgelesen werden.
 *    Die Datein mit der vorgegebenen Datenstruktur ist produkt.json.
 * <p>
 *    Den Code für bestimmte wiederkehrende Aufgaben (z.B. das Lesen einer ganzen Datei als String)
 *    benötigt man immer wieder
 *    Bemerken wir in unserem Projekt solchen wiederverwendbaren Hilfscode
 *    lösen wir ihn aus der Klasse heraus und legen ihn an einem zentralen Ort im Projekt ab
 *    Solche Hilfsklassen(utils) sollen von uns und unseren Kollegen langristig wiederverwendet werden,
 *    deshalb ist besonders wichtig, dass Dokument(Kommentare/JavaDoc)
 *    und Tests für diese Klassen gut sind, so dass wir sie später noch verstehen.
 *    utils innerhalb eines Projekts liegen in der Regel in einem eigenenen package,
 *    um sie strukturell vom eigentlichen Anwednungscode zu trennen
 *<p>
 *    Innerhalb einer Firma wird man auch wiederverwendbaren Code haben, der nicht nur innerhalb
 *    eines Projektes sondern in mehreren Projekten wiederverwendet werden kann.
 *    Der wird dann in einem eigenen Projekt abgelegt und als Bibliothek(library, lib) in die anderen Projekte eingebunden
 *    Darüber hinaus gibt es Code, der nicht nur innerhalb einer Firma, sondern von allen Java-Entwicklern verwendet werden kann.
 *    DIeser kann als OpenSource Bib veröffentlicht werden
 *    typ Bsp. dafür ost die commons library der Apache Software Foundation
 *
 *    Nun FileImport der apache commons lib anstelle unseres eignenen verwenden, da dieser Code in der Regel beser durchdacht und
 *    besser getestet ist
 *<p>
 *    "Maven Repository" ist eine Seite, auf der viele OpenSource libs erhältlich sind
 *    Seite ist ursprünglich für Nutzung via Maven gedacht gewesen
 *    Libs können aber auch manuell als .jar(Java Archieve) heruntergeladen werden
 *    Einbindung einer Third-Party-Lib
 *    Wenn unser Projekt eine lib benutzt, sagen wir, dass Projekt ist von lib abhängig
 *    ->lib ist eine Deendency des Projekts
 *    Die Art und Weise, eine Dependency innerhalb der IDE(IntelliJ, Eclipse) fest in Projekt
 *    einzubinden, ist erstmal recht einfach. Bei vielen Dependencies entsteht hier aber ein hoher Wartungsaufwand.
 *    Außerdem wirft das eine Frage beim comitten auf:
 *    Soll: die lib mit comitted werden oder nicht?
 *    ja?-> lib groß, com dauert lange, darf man fremde libs öffentlich comitten?
 *    nein?-> ohne lib ist es schwierig für andere Programm auszuführen, libs müssten dann extra eingebunden werden dependencies selbst heruntergeladen werden,
 *    das macht den Umgang schwer
 *
 *    Aus diesem Grund hat man Build Tools erfunden, die das Dependency Management erledigen können.
 *    JAva-Build-Tools dem Alter nach: Ants-Maven_Gradle_Ivy
 *    Wir benutzen Maven
 *<p>
 *     Am einfachsten ist es ein neues Projekt von Anfang an mit Maven anzulegen.
 *     Aber IntelliJ hilft hierbei zumeist, nachträglich ein Maven-Projekt zu machen
 *13. Refactoring
 *
 *
 *
 */


public class GetränkeAutomat {


    //Konsoleniengabe durch Nutzer
    private final Scanner scanner = new Scanner(System.in);
    //Liste aller verfügaren Produkte
    private List<Getränk> produktListe;
    //Liste der Zahlungsmittel, die der Automat akzeptiert
    private List<Coin> acceptedCoins;

    // Zähler für alle Einnahmen von Programmstart bis Ende
    private int tagesEinnahmen = 0;

    // Zahlenformat für Darstellung von Eurogeldbeträgen;
    // #.00 bedeutet beliebig viele Vorkommastellen, exakt 2 Nachkommastellen
    public static final DecimalFormat EURO_FORMAT = new DecimalFormat("#0.00");

    //run()Methode ist nun übersichtlich und abstrakt, und zeigt was unser Programm tut
    //Details jedes einzelnen Schrittes sind in eigenen Methoden verborgen
    private void run() throws IOException {
        // wir müssen die Exception einstellen,
        //die von Apache lib geworfen wird, kein Problem wir reichen einen eventuellen Fehler nur weiter..

        produktListe = new ProduktImport().importiereProdukte("produkt.json");

        erzeugeAkzeptierteMuenzen();

        runMainMenu();

        shutDownAutomate();
    }

    private void shutDownAutomate() {
        System.out.println();
        double einnahmenInEuro = this.tagesEinnahmen / 100.0;
        System.out.println("Der Automat hat heute " + EURO_FORMAT.format(einnahmenInEuro) + " Euro eingenommen");
    }

    private void runMainMenu() {
        int nutzerEingabe= -1;// Setze auf beliebigenm bedeutungslosen Wert
        while(nutzerEingabe!=99) {
            gibVerfügbareAuswahlAus();
            nutzerEingabe = getNutzerEingabe();

            if (isValidProductNumber(nutzerEingabe)) {
                Getränk gewaehltesGestraenk = produktListe.get(nutzerEingabe);
                bezahleGetraenk(gewaehltesGestraenk);
            } else {
                System.out.println("Ein Getränk mit dieser Nummer ist nicht verfügbar!");
            }
        }
    }

    private boolean isValidProductNumber(int nutzerEingabe) {
        return nutzerEingabe >= 0 && nutzerEingabe < produktListe.size();
    }

    private void bezahleGetraenk(Getränk gewaehltesGestraenk) {
        int guthaben = lasseNutzerGeldEingeben(gewaehltesGestraenk);


        if(guthaben >= gewaehltesGestraenk.getPreis()){
            //Verkaufe Fahrschein
            gibAusgewaehltesGetraenkAus(gewaehltesGestraenk);
            this.tagesEinnahmen = this.tagesEinnahmen + gewaehltesGestraenk.getPreis();
            guthaben = guthaben - gewaehltesGestraenk.getPreis();
        }
        gibRestguthabenAus(guthaben);
    }

    private void gibRestguthabenAus(int guthaben) {
        if (guthaben <= 0) return; //Aus Effizienzgründen: Wenn 0 Wehselgeld erreicht, überprüfe gar nicht erst die Münzen.

        List<Coin> moeglicheWechselgeldMuenzenAufsteigendNachWert = new ArrayList<>(this.acceptedCoins);
        Collections.sort(moeglicheWechselgeldMuenzenAufsteigendNachWert);
        // Schleife über alle Münzen, absteigend
        int muenzIndex = moeglicheWechselgeldMuenzenAufsteigendNachWert.size() - 1;
        while ( muenzIndex >= 0) {
            Coin currentCoin = moeglicheWechselgeldMuenzenAufsteigendNachWert.get(muenzIndex);

            if (passendeMuenze(guthaben, currentCoin)) {
                //Gib Münze aus und reduziere den restlichen Betrag
                System.out.println("Der Automat gibt eine Münze aus: " + currentCoin.getBezeichnung());
                guthaben = guthaben - currentCoin.getWert();
            } else {
                muenzIndex--;// Schaue zu nächstkleinerer Münze falls diese Münze ausgegeben werden konnte
            }
        }
    }

    /**
     * Münze ist dann passend, wenn <= Guthaben und Typ == COIN
     */

    private boolean passendeMuenze(int guthaben, Coin currentCoin) {
        return currentCoin.getWert() <= guthaben && currentCoin.getType().equals(CoinType.COIN);
    }

    private int lasseNutzerGeldEingeben(Getränk gewaehltesGestraenk) {

        int bezahlteSummeinCent = 0;
        while(bezahlteSummeinCent < gewaehltesGestraenk.getPreis()) {
            zeigeRestbetragAn(gewaehltesGestraenk, bezahlteSummeinCent);

            String eingeworfen = scanner.nextLine();

            if(eingeworfen.equalsIgnoreCase("abbrechen")) break;

            Coin eingeworfeneMuenze = erkenneMuenze(eingeworfen);
            bezahlteSummeinCent = bezahlteSummeinCent + eingeworfeneMuenze.getWert();
        }
        return bezahlteSummeinCent;
    }

    private void zeigeRestbetragAn(Getränk gewaehltesGestraenk, int bezahlteSummeinCent) {
        int nochZuZahlenInCent = gewaehltesGestraenk.getPreis() - bezahlteSummeinCent;
        double nochZuZahlenInEuro = nochZuZahlenInCent/100.0 ;
        System.out.println("Bitte bezahlen Sie " + EURO_FORMAT.format(nochZuZahlenInEuro) + " Euro");
    }

    private Coin erkenneMuenze(String eingeworfen) {
        for (Coin c : this.acceptedCoins) {
            if (c.getKuerzel().equals(eingeworfen)) {
                return c;
            }
        }

        System.out.println("Der Automat nimmt diese Münze nicht an.");
        return Coin.UNGUELTIGE_MUENZE;
    }

    private void erzeugeAkzeptierteMuenzen() {
        this.acceptedCoins = new ArrayList<>();
        this.acceptedCoins.add(new Coin("5 Cent", "5ct",5, CoinType.COIN));
        this.acceptedCoins.add(new Coin("10 Cent", "10ct",10, CoinType.COIN));
        this.acceptedCoins.add(new Coin("20 Cent", "20ct",20, CoinType.COIN));
        this.acceptedCoins.add(new Coin("50 Cent", "50ct",50, CoinType.COIN));
        this.acceptedCoins.add(new Coin("1 Euro", "1eur",100, CoinType.COIN));
        this.acceptedCoins.add(new Coin("2 Euro", "2eur",200, CoinType.COIN));
        this.acceptedCoins.add(new Coin("5 Euro", "5eur",500, CoinType.BILL));
        this.acceptedCoins.add(new Coin("10 Euro", "10eur",1000, CoinType.BILL));
        this.acceptedCoins.add(new Coin("20 Euro", "20eur",2000, CoinType.BILL));
    }

    private int getNutzerEingabe() {
        Scanner scanner = this.scanner;
        int eingabe = scanner.nextInt();
        scanner.nextLine();
        return eingabe;
    }

    private void gibAusgewaehltesGetraenkAus(Getränk gewaehltesGestraenk) {
        System.out.println();
        System.out.println("Der Automat gibt eine Flasche " + gewaehltesGestraenk.getBezeichnung() + " aus.");
    }

    private void gibVerfügbareAuswahlAus() {
        System.out.println();
        System.out.println("Bitte wählen Sie ein Getränk aus:");

        for (int i = 0; i < produktListe.size() ; i++) {
                System.out.println(i + "\t" + produktListe.get(i));
        }
    }

    //@Deprecated soll nicht mehr verwendet werden. Stattdesseneinlesen aus Datei
    @Deprecated
    private void erzeugeProduktDaten() {
        produktListe = new ArrayList<>();
        produktListe.add(new Getränk("Wasser", 160));
        produktListe.add(new Getränk("Fanta", 120));
        produktListe.add(new Getränk("Spezi", 180));
        produktListe.add(new Getränk("Saft", 250));
    }

    public static void main(String[] args) throws IOException {
        new GetränkeAutomat().run();
    }
}
