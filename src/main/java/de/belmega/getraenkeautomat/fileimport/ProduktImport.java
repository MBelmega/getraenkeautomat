package de.belmega.getraenkeautomat.fileimport;


import de.belmega.getraenkeautomat.Getränk;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProduktImport {


    public List<Getränk> importiereProdukte(String filename) throws IOException {
        File file = new File(filename);
        //Apache`s FileUtil ist schlauer als unsere; sie achten auf Encoding,
        //also den verwendeten Zeichensatz der Datei.
        // das ist besonders wichtig, wenn ausländische Schriftzeichen in der Datei sind.
        //In unserem Fall ist es egal, aber hier zeigt sich, dass die öffentlichen libs
        //auch SOnderzeichen abdecken, auf die wir garnicht gekommen wären
        String content = FileUtils.readFileToString(file, "utf-8");
        List<String> getraenkeAsString = findProductsInString(content);

        return erzeugeGetränkeAusString(getraenkeAsString);
    }

    private List<Getränk> erzeugeGetränkeAusString(List<String> getraenkeAsString) {
        List<Getränk> results = new LinkedList<>();

        for (String s: getraenkeAsString){
            results.add(createGetraenkFromString(s));
        }

        return results;

    }



    private static final String PRODUCT_PATTERN_STRING = "\\{.*\\}";
    //da { ein Steuerzeichen für regular expressions (RegEx) ist, muss \\ kenntlich gemacht werden, dass tatsächlich die geschweifte Klammer gemeint ist

    private static final Pattern PRODUCT_PATTERN = Pattern.compile(PRODUCT_PATTERN_STRING);
    //Java kompiliert jetzt das Muster von oben zu einem Pattern Object

    /**
     * Suche nach dem Muster{...}, also zwei geschweiften Klammern mit Text dazwischen.
     * Geschrieben als Regulärer Ausdruck:{ .* } Der Punkt steht für ein beliebiges Zeichen, das Sternchen bedeutet
     * "beliebig viele vom vorangegangenen Zeichen
     *
     * Wir nutzen Javas Bibliothek für Reguläre Ausdrücke, um unsere Suche zu implementieren.
     * @param content
     * @return
     */
    public List<String> findProductsInString(String content) {
        List<String> results = new LinkedList<>();

        Matcher matcher = (PRODUCT_PATTERN).matcher(content);//Nutze das oben definierte Pattern, um auf content nach Treffern zu suchen

        while(matcher.find())
            results.add(matcher.group());// Füge jedes Vorkommen eines passenden Musters der Liste hinzu

        return results;
    }
    // Der .* Operator ist "gierig" "greedy", er versucht so viele Zeichen wie möglich zu schlucken, deshalb geht er zwei Anführungszeichen weiter
    // Das ? macht den Operator genügsam: Er sucht jetzt nach dem kleinstmöglichen Treffer.
    private static String BEZEICHNUNG_PATTERN_STRING = "\"bezeichnung\" : \"(.*?)\"";
    private static Pattern BEZEICHNUNG_PATTERM = Pattern.compile(BEZEICHNUNG_PATTERN_STRING);

    private static String PREIS_PATTERN_STRING = "\"preis\" : (.*) \\}";
    private static Pattern PREIS_PATTERM = Pattern.compile(PREIS_PATTERN_STRING);

    public Getränk createGetraenkFromString(String productString) {
        Matcher bezeichnungMatcher = BEZEICHNUNG_PATTERM.matcher(productString);
        bezeichnungMatcher.find();
        String bezeichnung = bezeichnungMatcher.group(1); //gibt die erste Gruppe, d.h. den inhalt der ersen runden Klammer


        Matcher preisMatcher = PREIS_PATTERM.matcher(productString);
        preisMatcher.find();
        String preisInEuro  = preisMatcher.group(1);
        int preisInCent = Integer.parseInt(preisInEuro.replace(".","")); //EWrsetze Punkt durch nichts


        return new Getränk(bezeichnung, preisInCent);
    }

}
