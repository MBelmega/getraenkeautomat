package de.belmega.getraenkeautomat;
/**
 * Klasse für Münzen und Geldscheine.
 * implements Comparable<Coin> : Man kann eine Münze mit einer anderen vergleichen, es gibt eine klar definierte Ordnung
 */

public class Coin implements Comparable<Coin>{

    /**
     * Diese Konstante repräsentiert eine ungültige Münze, in gewissem Sinne "die Abwesenheit einer gültigen Münze".
     * Ein Objekt, das die Abwesenheit eines gültigen Objekts repäsentiert, nennt man NullObject.
     * Das NullObject ist ein besserer Umgang mit der Abwesenheit eones gültigen Objekts, als einfach return null zurückgeben!
     */
    public static final Coin UNGUELTIGE_MUENZE = new Coin("Ungültige Münze", "Ungültig", 0, CoinType.COIN);
    private final String bezeichnung;
    private final String kuerzel;
    private final int wert;
    private CoinType type;

    public Coin(String bezeichnung, String kuerzel, int wert, CoinType type) {
        this.bezeichnung = bezeichnung;
        this.kuerzel = kuerzel;
        this.wert = wert;
        this.type = type;
    }

    public String getKuerzel() {
        return kuerzel;
    }

    public int getWert() {
        return wert;
    }

    @Override
    public int compareTo(Coin otherCoin) {
        if(this.wert < otherCoin.wert)return -1;
        else if (this.wert > otherCoin.wert) return 1;
        else return 0;
        //Bedeutung: natürliche Sortierungsreihenfolge von Münzen ergibt sich aus ihrem Wert.
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true; // Jedes Object ist equal() mit sich selbst
        if (!(o instanceof Coin)) return false; // Das Object o kann nicht equal() sein, wenn es gar nicht zur Klasse Coin gehört

        Coin coin = (Coin) o; // Wenn o ein Coin ist.....

        if (wert != coin.wert) return false; //... prüfe Wert,
        if (!bezeichnung.equals(coin.bezeichnung)) return false; // Bezeichnung
        if (!kuerzel.equals(coin.kuerzel)) return false; // Kürzel
        return type == coin.type; // und Typ. Wenn alle der Eigenschadften gleich sind, return "true", sonst "false"

    }

    @Override
    public int hashCode() { // zwei Objects, die equal() sind, sollen laut JavaKonvention auch denselben HashCode haben
        int result = bezeichnung.hashCode();
        result = 31 * result + kuerzel.hashCode(); // Daher nutze wieder die vier oben genannten Eigenschaften,
        result = 31 * result + wert; // um einen Hashcode zu errechnen.
        result = 31 * result + type.hashCode();
        return result;
    }

    public CoinType getType() {
        return type;


    }
}
