package de.belmega.getraenkeautomat.utils;
//wenn eine Datei bei IntelliJ in ein Package verschoben wird
//wird die package-declaration ob in der Klasse automatisch generiert
// jedes Package mit"." Punkt ist ein eigenes Unterpackage
//packages dienen der Ortung und struktur des codes
//im dateisystem(windows) entspricht jedes package einem ordner mit unterordnern

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Utility class for File Operations
 */
//absract: keine Instanzen, nur eine Sammlung statischer Methoden
public abstract class FileUtil {

    private static final String END_OF_FILE = "\\A"; // Zeichen für Dateiende

    /**
     * Liest die übergebene Datei mit dem angegeben Dateinamen als String
     * @param file die zu lesende Datei
     * @return den Inhalt der Datei als String
     * @throws FileNotFoundException wenn die Datei nicht gefunden wurde
     * @deprecated Nutze org.apache.commons.io.FileUtils.readFileToString(File, Charset) instead
     */
    @Deprecated
    public static String readFile(File file) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        Scanner scanner = new Scanner(fileInputStream);

        String content = scanner.useDelimiter(END_OF_FILE).next();// Scanne den InputStream bis zum nächsten End of File => lies ganze Datei aus


        return content;
    }
}
