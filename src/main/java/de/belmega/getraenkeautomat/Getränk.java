package de.belmega.getraenkeautomat;
public class Getränk {



    private final String bezeichnung;
    private final int preis;

    public Getränk(String bezeichnung, int preisinCent) {
        this.bezeichnung = bezeichnung;
        this.preis = preisinCent;
    }

    @Override
    public String toString() {
        double preisInEuro = preis / 100.0;
        return  this.bezeichnung + "\t" + GetränkeAutomat.EURO_FORMAT.format(preisInEuro) + " Euro"; //\t ist tabulator
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public int getPreis() {
        return preis;
    }

    public String getPreisInEuro() {
        return Double.toString(preis / 100.0);
    }
}
